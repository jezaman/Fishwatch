# Fishwatch


## Getting started

To make it easy for you and to ensure an identical starting template, 
use `create-react-app`'s `typescript` template as a starting point:

```npx create-react-app fishwatch --template typescript```

## Goal

The goal of this test is to develop a simple, visually pleasing web app.  
This will enable us to get a better understanding of your programming skills and methodology.  

## Assignment

Build a single-page web app consisting of a grid layout of various fish that can be searched and filtered.  
For this you can use the free API `https://www.fishwatch.gov/api/species`  
More info can be found at https://www.fishwatch.gov/developers  

This application must have the following minimum features:

1) A grid layout containing the various fish. Each tile should show at least:
   1) One photo of the species
   2) The species name
   3) The harvest type. `Wild` and `Farmed` fish should have a visually different grid tile. 
   4) Feel free to use other data fields if it suits your UI. 
   

2) Search bar that filters the various fish based on their species name  


3) A select / dropdown to filter species based on harvest type (`Wild`, `Farmed`, or any)


4) Clicking on an individual tile should reveal some more information about the species. You are free to select the way this extra information is shown (e.g. modal, other "page", ..).   

If you have some time left, you can add extra features or UI enhancements as desired.  
Just remember, this assignment should only take max. 2 hour of your time. 


## Rules

- **You have 2.5 hours to complete this task**. This gives you some extra time to read this document and take a break when needed. 
- You can use any modules or tools as you see fit, just make sure your own code is still representative.
- You can style the app to your own preference.
- If you have any technical issues (e.g. Murphy's Law causing the Fishwatch API to go down, etc.) feel free to send us a message on LinkedIn

## Submission
Submit your code by sending a link to your Git repository in an email to `florian@weave.ly` and `jesse@weave.ly`. 
Any additional information documenting your methodology can be attached in the email.
